/*********************************************************************
 * File  : ciudad.cpp
 * Date  : 2014
 *********************************************************************/

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <cstdlib>  // Para establecer la semilla srand() y generar números aleatorios rand()
#include <limits>
#include <math.h>
#include <string.h>
#include <stdio.h>
#include <ctime>
#include "ciudad.h"

using namespace IG;
using namespace std;

	// Reservar memoria para la matriz en la que por defecto todos son edificios (0).
    // x y z corresponde con el ancho y el largo de la ciudad
	int Ciudad::inicializar(int x, int z) {
		int cont = 0, cont2 = 0;

		/* RESERVA MEMORIA PARA EL VECTOR DE TIPOS DE ELEMENTOS DE MANZANAS */
		inicializarVectorSeleccion();

		/* RESERVA MEMORIA PARA LA MATRIZ DE CALLES */			
		_matriz = new int * [x];

    	for(cont = 0; cont < x; ++cont)
			_matriz[cont] = new int[z]; // Los datos que hay por cada linea

		// Bordes de la matriz con su respectivas rotaciones y tipos según se indica en _manzanasSeleccion
		_matriz[0][0] = 0;
		_matriz[0][z - 1] = 1;
		_matriz[x - 1][0] = 2;
		_matriz[x - 1][z - 1] = 3;

		// Inicializamos las manzanas a un estado imposible que no existe, pero mejor que dejar basura de la inicialización
		for(cont = 1; cont < (x - 1); ++cont)
			for(cont2 = 1; cont2 < (z - 1); ++cont2)
				_matriz[cont][cont2] = -1;

		// Igual que antes tipos especiales de bordes que tiene su correspondencia en _manzanasSeleccion
		for(cont = 1; cont < (x - 1); ++cont)
			_matriz[cont][0] = 4;

		for(cont = 1; cont < (x - 1); ++cont)
			_matriz[cont][z - 1] = 5;

		for(cont2 = 1; cont2 < (z - 1); ++cont2)
			_matriz[0][cont2] = 6;

		for(cont2 = 1; cont2 < (z - 1); ++cont2)
			_matriz[x - 1][cont2] = 7;

		return 0;
	}


	void Ciudad::inicializarVectorSeleccion() {
		int cont = 0, cont2 = 0, numPermitidosDerecha = 0, numPermitidosAbajo = 0, numPermitidosIzquierda = 0, numPermitidosArriba = 0, tamanio = 0;
		ifstream fichero("plantillas/ficheros configuracion/manzanas+orientaciones.txt", ifstream::in);
		string cadena;

		if (!fichero) {
			cout << "-1 error al abrir el fichero" << "\n";
			exit(-1);
		}
			
		// Leemos el numero de manzanas que hay en el fichero
		fichero >> tamanio;

		_manzanasSeleccion = new losaOrientacion[tamanio];
		
		// Leemos el size del solar
		fichero >> _tam_solar;

		// Leemos la altura de los niveles
		fichero >> _alt_nivel;
		
		// Leemos la altura de la base
		fichero >> _alt_base;

		// Leemos el contenido de los tipos de manzanas y sus orientaciones		
		getline(fichero, cadena);		

		for(cont = 0; cont < tamanio; ++cont) {
			// Leemos el tipo y orientación de una manzana
			fichero >> _manzanasSeleccion[cont].tipo;
			fichero >> _manzanasSeleccion[cont].orientacion;

			/* VECTOR DE PERMITIDOS DERECHA */
			fichero >> numPermitidosDerecha; // Leemos el tamaño del vector y reservamos memoria
			_manzanasSeleccion[cont].permitidosDerecha = new int[numPermitidosDerecha + 1]; // Reservar uno más para el tamaño del vector
			_manzanasSeleccion[cont].permitidosDerecha[0] = numPermitidosDerecha; // Guardamos el tamaño del vector en la primera posición del vector
			for(cont2 = 1; cont2 <= numPermitidosDerecha; ++cont2)
				fichero >> _manzanasSeleccion[cont].permitidosDerecha[cont2]; // Leemos el resto de los datos hasta el tamaño del vector

			/* VECTOR DE PERMITIDOS ABAJO */
			fichero >> numPermitidosAbajo; // Leemos el tamaño del vector y reservamos memoria
			_manzanasSeleccion[cont].permitidosAbajo = new int[numPermitidosAbajo + 1]; // Reservar uno más para el tamaño del vector
			_manzanasSeleccion[cont].permitidosAbajo[0] = numPermitidosAbajo; // Guardamos el tamaño del vector en la primera posición del vector
			for(cont2 = 1; cont2 <= numPermitidosAbajo; ++cont2)
				fichero >> _manzanasSeleccion[cont].permitidosAbajo[cont2]; // Leemos el resto de los datos hasta el tamaño del vector

			/* VECTOR DE PERMITIDOS IZQUIERDA */
			fichero >> numPermitidosIzquierda; // Leemos el tamaño del vector y reservamos memoria
			_manzanasSeleccion[cont].permitidosIzquierda = new int[numPermitidosIzquierda + 1]; // Reservar uno más para el tamaño del vector
			_manzanasSeleccion[cont].permitidosIzquierda[0] = numPermitidosIzquierda; // Guardamos el tamaño del vector en la primera posición del vector
			for(cont2 = 1; cont2 <= numPermitidosIzquierda; ++cont2)
				fichero >> _manzanasSeleccion[cont].permitidosIzquierda[cont2]; // Guardamos el tamaño del vector en la primera posición del vector

			/* VECTOR DE PERMITIDOS ARRIBA */
			fichero >> numPermitidosArriba; // Leemos el tamaño del vector y reservamos memoria
			_manzanasSeleccion[cont].permitidosArriba = new int[numPermitidosArriba + 1]; // Reservar uno más para el tamaño del vector
			_manzanasSeleccion[cont].permitidosArriba[0] = numPermitidosArriba; // Guardamos el tamaño del vector en la primera posición del vector
			for(cont2 = 1; cont2 <= numPermitidosArriba; ++cont2)
				fichero >> _manzanasSeleccion[cont].permitidosArriba[cont2]; // Guardamos el tamaño del vector en la primera posición del vector
			
			// Almacenamos si el solar es edificable
			fichero >> _manzanasSeleccion[cont].edificable;
		} // end_for

		fichero.close(); // Cerramos el fichero
	}


	int * IG::interseccionVectores(int * vector1, int * vector2) {
		int cont = 0, cont2 = 0, cont3 = 1, tam = 0;
		int * resultado;

		// Damos una primera pasada para hallar la longitud del vector resultante cuando comparemos los elementos comunes
		for(cont = 1; cont <= vector1[0]; ++cont)
			for(cont2 = 1; cont2 <= vector2[0]; ++cont2)
				if(vector1[cont] == vector2[cont2])
					++tam;

		// Reservamos memoria para el vector resultante
		resultado = new int[tam + 1];			

		// Guardamos el tamaño del vector en la primera posición
		resultado[0] = tam;			

		// Guardamos los elemnentos comunes
		for(cont = 1; cont <= vector1[0]; ++cont)
			for(cont2 = 1; cont2 <= vector2[0]; ++cont2)
				if(vector1[cont] == vector2[cont2]) {
					resultado[cont3] = vector1[cont];
					++cont3; // Iterador para las posiciones del nuevo vector
				}

		return resultado;
	}


	void Ciudad::aleatorizarCalles() {
		int cont = 0, cont2 = 0;
		int * manzanasPermitidas;

		srand(time(NULL));

		for(cont = 1; cont < _ancho - 1; ++cont) {
			for(cont2 = 1; cont2 < _largo - 1; ++cont2) {
				// Celda superior y celda izquierda
				manzanasPermitidas = interseccionVectores(_manzanasSeleccion[_matriz[cont - 1][cont2]].permitidosAbajo, _manzanasSeleccion[_matriz[cont][cont2 - 1]].permitidosDerecha);

				// Celda derecha
				if (_matriz[cont][cont2 + 1] != -1)
					manzanasPermitidas = interseccionVectores(manzanasPermitidas, _manzanasSeleccion[_matriz[cont][cont2 + 1]].permitidosIzquierda);

				// Celda inferior
				if (_matriz[cont + 1][cont2] != -1)
					manzanasPermitidas = interseccionVectores(manzanasPermitidas, _manzanasSeleccion[_matriz[cont + 1][cont2]].permitidosArriba);

				_matriz[cont][cont2] = manzanasPermitidas[(1 + (rand() % (manzanasPermitidas[0] + 1 -1)))];

				delete [] manzanasPermitidas; // Liberamos la memoria del vector para la siguiente iteracion
			} // end_for_cont2
		} // end_for_cont
	}


	void Ciudad::callesTox3d(const char * archivo) {
		int cont = 0, cont2 = 0;
		char ficheroAux[(strlen(archivo)-1)*2+17];//="aux_calles_" + archivo + ".x3d";
		sprintf(ficheroAux, "%s/aux_calles_%s.x3d", archivo, archivo);
		ofstream fichero(ficheroAux, ofstream::out);//flujo de salida del fichero

		fichero << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<X3D profile=\"Immersive\" version=\"3.0\">\n\n";
		fichero << "<Scene>\n\t<Group DEF=\"Calles3D\">\n";

		for(cont = 0; cont < _ancho; ++cont) {
			for(cont2 = 0; cont2 < _largo; ++cont2) {
				/* Colocamos los translations */
				fichero << "\n\t\t<Transform translation=\"" << (cont2 * _tam_solar) << " 0 " << (cont * _tam_solar) << "\" ";

				/* Comprobamos las rotaciones */
				if(_manzanasSeleccion[_matriz[cont][cont2]].orientacion == 0)
					fichero << ">\n";
				else
					fichero << "rotation=\"0.0 1.0 0.0 " << _manzanasSeleccion[_matriz[cont][cont2]].orientacion << "\">\n";					

				/* Buscamos cual es el objeto que tenemos que insertar */
				fichero << "\t\t\t<Inline url=\"../plantillas/elementos_construccion/" << _manzanasSeleccion[_matriz[cont][cont2]].tipo << ".x3d\"/>\n\t\t</Transform>\n";
			} // for cont2
		} // for cont		

		fichero << "\t</Group>\n</Scene>\n</X3D>";

		fichero.close(); //Cerramos el fichero
	}


	void Ciudad::edificiosTox3d(const char * archivo) {
		int cont = 0, cont2 = 0, cont3 = 0, altura;

		char ficheroAux[(strlen(archivo)-1)*2+20];//="aux_calles_" + archivo + ".x3d";
		sprintf(ficheroAux, "%s/aux_edificios_%s.x3d", archivo, archivo);
		ofstream fichero(ficheroAux, ofstream::out);//flujo de salida del fichero

		fichero << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<X3D profile=\"Immersive\" version=\"3.0\">"<<"\n\n";
		fichero << "<Scene>\n\t<Group DEF=\"Edificios3D\">"<<"\n";

		for(cont = 1; cont < _ancho - 1; ++cont) {
			for(cont2 = 1; cont2 < _largo - 1; ++cont2) {
				// Si es posible construir en la manzana
				if (_manzanasSeleccion[_matriz[cont][cont2]].edificable == 1) {
					altura = (_altura_min + (rand()% (_altura_max + 1 - _altura_min)));

					/* -------------------------------------- COLOCACION DE NIVELES --------------------------------------------*/
					for(cont3 = 0; cont3 < altura; ++cont3) {
						/* Colocamos el tranlation de los niveles */
						fichero << "\n\t\t<Transform translation=\"" << (cont2 * _tam_solar) << " " << ((cont3 * _alt_nivel) + _alt_base) << " " << (cont * _tam_solar) << "\" "; 

						/* Comprobamos la rotacion de los niveles*/
						if(_manzanasSeleccion[_matriz[cont][cont2]].orientacion == 0)
							fichero << ">\n";
						else
							fichero << "rotation=\"0.0 1.0 0.0 " << _manzanasSeleccion[_matriz[cont][cont2]].orientacion << "\">\n";					

						/* Buscamos el nivel a insertar */
						fichero << "\t\t\t<Inline url=\"../plantillas/elementos_construccion/nivel_" << _manzanasSeleccion[_matriz[cont][cont2]].tipo << ".x3d\"/>\n\t\t</Transform>\n";
					}
					
					/* -------------------------------------- COLOCACION DE AZOTEAS ---------------------------------------------*/
					if(_decoracion)
					{
						/* Buscamos cual es el objeto que tenemos que insertar */
						fichero << "\n\t\t<Transform translation=\"" << (cont2 * _tam_solar) << " " << (cont3 * _alt_nivel) << " " << (cont * _tam_solar) << "\" ";
					
						/* Comprobamos la rotacion de las azoteas*/
						if(_manzanasSeleccion[_matriz[cont][cont2]].orientacion == 0)
							fichero << ">\n";
						else
							fichero << "rotation=\"0.0 1.0 0.0 " << _manzanasSeleccion[_matriz[cont][cont2]].orientacion << "\">\n";
					
						/* Buscamos la azotea a insertar */
						fichero << "\t\t\t<Inline url=\"../plantillas/elementos_construccion/azotea_" << _manzanasSeleccion[_matriz[cont][cont2]].tipo << ".x3d\"/>\n\t\t</Transform>\n";
					}

					/* -------------------------------------- COLOCACION DE BASES -----------------------------------------------*/
					/* Colocamos el tranlation de las bases */
					fichero << "\n\t\t<Transform translation=\"" << (cont2 * _tam_solar) << " 0 " << (cont * _tam_solar) << "\" ";

					/* Comprobamos la rotacion de las bases*/
					if(_manzanasSeleccion[_matriz[cont][cont2]].orientacion == 0)
						fichero << ">\n";
					else
						fichero << "rotation=\"0.0 1.0 0.0 " << _manzanasSeleccion[_matriz[cont][cont2]].orientacion << "\">\n";					

					/* Buscamos la base a insertar */
					fichero << "\t\t\t<Inline url=\"../plantillas/elementos_construccion/base_" << _manzanasSeleccion[_matriz[cont][cont2]].tipo << ".x3d\"/>\n\t\t</Transform>\n";
				}
			} // for cont2
		} // for cont		

		fichero<<"\t</Group>\n</Scene>\n</X3D>";
		
		fichero.close();//cerramos el fichero
	}


	void Ciudad::decoracionTox3d(const char * archivo) {
		int cont = 0, cont2 = 0;
		char ficheroAux[(strlen(archivo) - 1) * 2 + 21]; //="aux_calles_" + archivo + ".x3d";
		sprintf(ficheroAux, "%s/aux_decoracion_%s.x3d", archivo, archivo);
		ofstream fichero(ficheroAux, ofstream::out); //flujo de salida del fichero

		fichero << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<X3D profile=\"Immersive\" version=\"3.0\">\n\n";
		fichero << "<Scene>\n\t<Group DEF=\"Edificios3D\">\n";

		for(cont = 1; cont < _ancho - 1; ++cont) {
			for(cont2 = 1; cont2 < _largo - 1; ++cont2) {
				fichero << "\n\t\t<Transform translation=\"" << ((cont2 * _tam_solar) + 6) << " " << (0.5) << " " << ((cont * _tam_solar) + 4) << "\" >\n"; 

				/* Buscamos cual es el objeto que tenemos que insertar */
				fichero << "\t\t\t<Inline url=\"../plantillas/decoracion/farola.x3d\"/>\n\t\t</Transform>\n";

				if(((double) rand() / ((double)RAND_MAX)) > 0.5) {
					fichero << "\n\t\t<Transform translation=\"" << ((cont2 * _tam_solar) + 6) << " " << (0.4) << " " << ((cont * _tam_solar) - 4) << "\" rotation=\"0.0 1.0 0.0 1.5707\">\n";

					/* Buscamos cual es el objeto que tenemos que insertar */	
					fichero << "\t\t\t<Inline url=\"../plantillas/decoracion/papelera.x3d\"/>\n\t\t</Transform>\n";
				}
			} // for cont2
		} // for cont		

		fichero << "\n\t\t<Transform translation=\"" << (_largo * _tam_solar / 2) << " " << (2 * ((_altura_max * _alt_nivel) + _alt_base)) << " " << (_ancho * _tam_solar / 2) << "\">\n"; 
		fichero << "\t\t\t<Inline url=\"../plantillas/edificios/Zeppelin.x3d\"/>\n\t\t</Transform>\n";

		fichero << "\t</Group>\n</Scene>\n</X3D>";

		fichero.close(); // Cerramos el fichero
	}


	void Ciudad::generateX3d(const char * archivo) {
		char ficheroFinal[(strlen(archivo) - 1) * 2 + 6]; //="aux_calles_" + archivo + ".x3d";
		sprintf(ficheroFinal, "%s/%s.x3d", archivo, archivo);
		ofstream fichero(ficheroFinal, ofstream::out); //flujo de salida del fichero

		fichero << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<X3D profile=\"Immersive\" version=\"3.0\">\n\n";
		fichero << "<Scene>\n\t<Group DEF=\"Ciudad3D\">\n";


		/* Incluimos los puntos de vista */
		fichero << "\n\t\t<Transform translation=\"" << (_largo * _tam_solar / 2) << " " << (4 * ((_altura_max * _alt_nivel) + _alt_base)) << " " << ((_ancho * _tam_solar) + (_ancho * (_tam_solar / 2))) << "\">\n"; 
		fichero << "\t\t\t<Viewpoint description=\"Sur\" orientation=\"1 0 0 -0.66325\"/>\n\t\t</Transform>\n";

		fichero << "\n\t\t<Transform translation=\"" << ((_largo * _tam_solar) + (_largo * (_tam_solar / 2))) << " " << (4 * ((_altura_max * _alt_nivel) + _alt_base)) << " " << (_ancho * _tam_solar / 2) << "\" rotation=\" 0 1 0 1.5707\">\n"; 
		fichero << "\t\t\t<Viewpoint description=\"Este\" orientation=\"1 0 0 -0.66325\"/>\n\t\t</Transform>\n";

		fichero << "\n\t\t<Transform translation=\"" << (_largo * _tam_solar / 2) << " " << (4 * ((_altura_max * _alt_nivel) + _alt_base)) << " " << -(_ancho * _tam_solar / 2) << "\" rotation=\" 0 1 0 3.1415\">\n"; 
		fichero << "\t\t\t<Viewpoint description=\"Norte\" orientation=\"1 0 0 -0.66325\"/>\n\t\t</Transform>\n";

		fichero << "\n\t\t<Transform translation=\"" << -(_largo * _tam_solar / 2) << " " << (4 * ((_altura_max * _alt_nivel) + _alt_base)) << " " << (_ancho * _tam_solar / 2) << "\" rotation=\"0 1 0 -1.5707\">\n"; 
		fichero << "\t\t\t<Viewpoint description=\"Oeste\"  orientation=\"1 0 0 -0.66325\"/>\n\t\t</Transform>\n\n";

		fichero << "\n\t\t<Viewpoint description=\"Superior\" position=\"" << (_largo * _tam_solar / 2) << " " << (8 * ((_altura_max * _alt_nivel) + _alt_base)) << " " << (_ancho * _tam_solar / 2) << "\" orientation=\"1 0 0 -1.6707963267\"/>\n";

		/* Incluimos el suelo */
		fichero << "\t\t<Inline url=\"aux_calles_" << archivo << ".x3d\"/>\n";

		/* Incluimos los edificios */
		fichero << "\t\t<Inline url=\"aux_edificios_" << archivo << ".x3d\"/>\n";

		/* Incluimos la decoracion */
		if(_decoracion)
			fichero << "\t\t<Inline url=\"aux_decoracion_" << archivo << ".x3d\"/>\n";

		fichero << "\t</Group>\n</Scene>\n</X3D>";

		fichero.close(); // Cerramos el fichero
	}

	// Liberar memoria para las estructuras de datos
	void Ciudad::liberarMemoria() {
		int cont = 0;

		for(cont = 0; cont < _ancho; ++cont)
			delete [] _matriz[cont];
	
		delete [] _matriz;

		delete [] _manzanasSeleccion->permitidosDerecha;
		delete [] _manzanasSeleccion->permitidosAbajo;
		delete [] _manzanasSeleccion->permitidosIzquierda;
		delete [] _manzanasSeleccion->permitidosArriba;

		delete [] _manzanasSeleccion;
	}
