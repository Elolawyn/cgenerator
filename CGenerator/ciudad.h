/*********************************************************************
 * File  : ciudad.h
 * Date  : 2014
 *********************************************************************/

#ifndef _CIUDAD_H_
#define _CIUDAD_H_

namespace IG
{
	struct losaOrientacion 
	{
		char  tipo[255];			// Tipo de manzana de todas las disponibles que hay. Tambien es parte del nombre del fichero 3d a usar.
		float orientacion;			// Orientación para ayudar a la rotación y la generación de calles aleatorias.
		int * permitidosDerecha;	// Manzanas permitidas a la derecha de esta manzana.
		int * permitidosAbajo;		// Manzanas permitidas abajo de esta manzana.
		int * permitidosIzquierda;	// Manzanas permitidas a la izquierda de esta manzana.
		int * permitidosArriba;		// Manzanas permitidas arriba de esta manzana.
		int   edificable;			// Se permite construir en la manzana.
	};

	class Ciudad 
	{
		private:

			void liberarMemoria();
			
		public:
		
			int					_ancho;				// Ancho de la ciudad definido por el usuario(z)
			int					_largo;				// Largo de la ciudad definido por el usuario(x)
			bool				_decoracion;		// Activacion de elementos decorativos.
			losaOrientacion *	_manzanasSeleccion;	// Vector que contendrá todos los tipos de losas y sus orientaciones.
			int **				_matriz;			// El entero intica un índice del vector de manzanasSeleccion.
			float				_tam_solar;			// Size del solar. Cada solar debe ser cuadrado.
			float				_alt_nivel;			// Altura de los niveles.
			float				_alt_base;			// Altura de la base de los edificios.
			int                 _altura_min;        // Altura minima de los edificios.
			int					_altura_max;		// Altura maxima de los edificios.			
			// Constructor
			Ciudad()
			{
				_ancho      = 1;
				_largo      = 1;
				_altura_min = 3;
				_decoracion = false;
			};

			// Destructor
			~Ciudad()
			{
				liberarMemoria();
			};

			// Reservar memoria para la matriz
			int inicializar(int x, int z);
			void inicializarVectorSeleccion();
			
			void aleatorizarCalles();
			void generateX3d(const char * archivo); 
			void callesTox3d(const char * archivo); 
			void edificiosTox3d(const char * archivo); 
			void decoracionTox3d(const char * archivo); 
	};//end_class

	/* Función auxiliar que está fuera de la clase ciudad pero dentro del espacio de nombre IG */
	int * interseccionVectores(int * vector1, int * vector2);

}; //end_namespace

#endif
