#include <QMainWindow>
#include <QAction>
#include <QMenu>
#include <QAction>
#include <QApplication>
#include <QButtonGroup>
#include <QDoubleSpinBox>
#include <QHeaderView>
#include <QLabel>
#include <QLineEdit>
#include <QMainWindow>
#include <QMenuBar>
#include <QPushButton>
#include <QRadioButton>
#include <QCheckBox>
#include <QSpinBox>
#include <QWidget>




#ifndef _INTERFAZ_H_
#define _INTERFAZ_H_


class Interfaz: public QMainWindow
{
Q_OBJECT

 public:
   Interfaz();
  ~Interfaz();

 private:
   QMenu *menuSobre;
             //Para hacer un men� necesito definir un 
             // objeto de la clase QMenu y las acciones (QAction)
             // que se ejecutar�n al seleccionar las opciones del men�.
  QAction *menuHelpAbout, *menuHelpAboutQt;

private slots:
 
  void parserAndExecutingCGeneratorAlgorithm();
  void about();
  void aboutQt();
 
private:

		QWidget *centralwidget;

		QRadioButton *boton_decoracion;

		QCheckBox *boton_extension;

		QLineEdit *cuadro_fichero_salida;

		QLabel *label_ancho, *label_largo, *label, *label_fichero_salida;

		QPushButton *boton_generar;

		QSpinBox *spinBox, *spinBox_2;

		QDoubleSpinBox *doubleSpinBox;

};

#endif
