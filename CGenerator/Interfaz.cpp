#include <QMenuBar>
#include <QStatusBar>
#include <QMessageBox>
#include <QtCore/QCoreApplication>

#include <iostream>
#include <string.h>
#include <stdio.h>
#include <cstdlib>

#include "Interfaz.h"
#include "ciudad.h"


using namespace IG;
using namespace std;

Interfaz::Interfaz()
{
// Ahora voy a declarar todo lo que tendr� mi ventana
// principal, en este caso un men�. Para ello primero
// declaro las acciones que habr� en el men�.

	menuHelpAbout = new QAction(tr("Autores y Licencia"), this);
  connect(menuHelpAbout, SIGNAL(triggered()), this, SLOT(about()));
	menuHelpAboutQt = new QAction(tr("Licencia Qt"), this);
  connect(menuHelpAboutQt, SIGNAL(triggered()), this, SLOT(aboutQt()));

	menuSobre = menuBar()->addMenu(tr("Sobre..."));

	menuSobre->addAction(menuHelpAbout);
	menuSobre->addAction(menuHelpAboutQt);


	resize(432, 300);
	setMinimumSize(QSize(432, 300));
	setMaximumSize(QSize(432, 300));

  QIcon icon;
  icon.addFile("plantillas/iconos/Logotipo City-Generator.png", QSize(), QIcon::Normal, QIcon::Off);
  setWindowIcon(icon);

  centralwidget = new QWidget(this);
  centralwidget->setObjectName("centralwidget");

  boton_decoracion = new QRadioButton(centralwidget);
  boton_decoracion->setObjectName("boton_decoracion");
  boton_decoracion->setGeometry(QRect(110, 170, 200, 17));

	boton_extension = new QCheckBox(centralwidget);
  boton_extension->setObjectName("boton_extension");
  boton_extension->setGeometry(QRect(330, 120, 70, 20));
	boton_extension->setChecked(true);
	boton_extension->setEnabled(false);



   doubleSpinBox = new QDoubleSpinBox(centralwidget);
   doubleSpinBox->setObjectName("doubleSpinBox");
   doubleSpinBox->setGeometry(QRect(140, 87, 81, 22));
   doubleSpinBox->setMinimum(3);
   doubleSpinBox->setMaximum(16);

   cuadro_fichero_salida = new QLineEdit(centralwidget);
   cuadro_fichero_salida->setObjectName("cuadro_fichero_salida");
   cuadro_fichero_salida->setGeometry(QRect(140, 120, 180, 20));

   label_ancho = new QLabel(centralwidget);
   label_ancho->setObjectName("label_ancho");
   label_ancho->setGeometry(QRect(20, 30, 120, 20));

   boton_generar = new QPushButton(centralwidget);
   boton_generar->setObjectName("boton_generar");
   boton_generar->setGeometry(QRect(100, 230, 211, 23));

   label_largo = new QLabel(centralwidget);
   label_largo->setObjectName("label_largo");
   label_largo->setGeometry(QRect(20, 60, 120, 20));

   label = new QLabel(centralwidget);
   label->setObjectName("label");
   label->setGeometry(QRect(20, 90, 120, 20));

   spinBox = new QSpinBox(centralwidget);
   spinBox->setObjectName("spinBox");
   spinBox->setGeometry(QRect(140, 27, 81, 22));
   spinBox->setMinimum(1);
   spinBox->setMaximum(2147483647);

   spinBox_2 = new QSpinBox(centralwidget);
   spinBox_2->setObjectName("spinBox_2");
   spinBox_2->setGeometry(QRect(140, 57, 81, 22));
   spinBox_2->setMinimum(1);
   spinBox_2->setMaximum(2147483647);

   label_fichero_salida = new QLabel(centralwidget);
   label_fichero_salida->setObjectName("label_fichero_salida");
   label_fichero_salida->setGeometry(QRect(20, 120, 120, 20));

   setCentralWidget(centralwidget);



   QWidget::setTabOrder(spinBox, spinBox_2);
   QWidget::setTabOrder(spinBox_2, doubleSpinBox);
   QWidget::setTabOrder(doubleSpinBox, cuadro_fichero_salida);
   QWidget::setTabOrder(cuadro_fichero_salida, boton_decoracion);
   QWidget::setTabOrder(boton_decoracion, boton_generar);


   setWindowTitle(QApplication::translate("MainWindow", "CGenerator", 0));
   boton_decoracion->setText(QApplication::translate("MainWindow", "Activar embellecimiento", 0));
   boton_extension->setText(QApplication::translate("MainWindow", ".X3D", 0));
   //  boton_decoracion->setText(QApplication::translate("MainWindow", "Activar tramoya", 0));
   label_ancho->setText(QApplication::translate("MainWindow", "Ancho ciudad", 0));
   boton_generar->setText(QApplication::translate("MainWindow", "Generar", 0));
   label_largo->setText(QApplication::translate("MainWindow", "Largo ciudad", 0));
   label->setText(QApplication::translate("MainWindow", "Altura edificios", 0));
   label_fichero_salida->setText(QApplication::translate("MainWindow", "Fichero de salida", 0));


  connect(boton_generar, SIGNAL(released()),this, SLOT(parserAndExecutingCGeneratorAlgorithm()));
}
 




void Interfaz::parserAndExecutingCGeneratorAlgorithm()
{
int sysResult=0;


	// Si no se ha especificado nombre de fichero
	if(strcmp((cuadro_fichero_salida->text()).toUtf8(), "")==0)
	{
		statusBar()->showMessage(tr("ERROR NECESARIO NOMBRE O RUTA DE FICHERO"), 2000);
	}

	else
	{
		Ciudad ejemplo;

		ejemplo._ancho = spinBox_2->value() + 2;
		ejemplo._largo = spinBox->value() + 2;
		ejemplo._decoracion = boton_decoracion->isChecked();
		ejemplo._altura_max = doubleSpinBox->value();

	
		ejemplo.inicializar(ejemplo._ancho, ejemplo._largo);

		//CREACION DE DIRECTORIO PARA SOLUCION
		char comandoCarpeta[(strlen(((cuadro_fichero_salida->text()).toUtf8()).data())-1)+7];
		sprintf(comandoCarpeta, "mkdir \"%s\"", ((cuadro_fichero_salida->text()).toUtf8()).data());

		sysResult=system(comandoCarpeta); 

		ejemplo.aleatorizarCalles();
		ejemplo.callesTox3d(((cuadro_fichero_salida->text()).toUtf8()).data());
		ejemplo.edificiosTox3d(((cuadro_fichero_salida->text()).toUtf8()).data());

		if(ejemplo._decoracion)
			ejemplo.decoracionTox3d(((cuadro_fichero_salida->text()).toUtf8()).data()); 

		ejemplo.generateX3d(((cuadro_fichero_salida->text()).toUtf8()).data());

		//en caso de exito
		statusBar()->showMessage(tr("Fichero Generado"), 2000);
	}//end else
}


 

void Interfaz::about()
{
  QMessageBox::about( this, "Info. Autores y Licencia", 
			"Copyright (c) 2014, Arroyo Lubi�n Ra�l, Rodr�guez Lozano Francisco Javier \n\n All rights reserved. \n\n\n Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met: \n\n * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer. \n\n * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution. \n\n * Neither the name of copyright holders nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission. \n\n THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS \"AS IS\" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.");
}




void Interfaz::aboutQt()
{
  QMessageBox::aboutQt( this, "Ejemplo Qt" );
}



Interfaz::~Interfaz()
{
    // Este es el destructor de la clase.
    // Los widgets (en Qt widgets son los controles, es decir, men�s, botones, etc...) 
    // declarados con new en esta clase son hijos de ella.
    // En Qt no es necesario liberar con delete los widgets hijos de otro widget
    // al salir del mismo; Qt lo hace por nosotros automaticamente. Por eso siempre
    // usaremos punteros y new para declarar nuevos widgets de Qt.
}
