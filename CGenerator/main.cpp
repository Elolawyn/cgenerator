#include <QApplication>
#include "Interfaz.h"  // Fichero donde est� definida la clase Interfaz




int main( int argc, char ** argv ) 
{
    QApplication app( argc, argv );

    Interfaz * miventana = new Interfaz();
                   //Ahora la ventana principal no va a ser de tipo QMainWindow, va 
                   // a ser la mia propia que voy a crear declarando un objeto de la 
                   // clase Interfaz definida en el fichero Interfaz.h

    miventana->show();

    app.connect( &app, SIGNAL(lastWindowClosed()), &app, SLOT(quit()) );
    return app.exec();
}
